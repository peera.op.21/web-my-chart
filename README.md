# Web My Chart



## Getting started

Clone repository and initial database on docker
```
git clone https://gitlab.com/peera.op.21/web-my-chart.git
cd web-my-chart\WebAPI
docker-compose up -d

```

## Run web api
Open the project solution and run the WebAPI project.
After running the project, you will see the Swagger page. Please execute the endpoint /api/Seed/generate-data. to create sample data.
You will then see another endpoint. which works as follows
 - `/api/Production/estimate` (for showing all estimate data)
 - `/api/Production/estimate/year/{year}` (for showing sum production data by year, like the picture in the exam)
 - `/api/Production/estimate/insert` (for adding data)
 - `/api/Production/estimate/update` (for updating data)
 - `/api/Production/estimate/delete` (for erasing data)