using DataAccess.EFCore.InterfaceRepositories;
using DataAccess.EFCore.Models;
using DataAccess.EFCore.Repositories;
using Microsoft.EntityFrameworkCore;
using Services;
using Services.InterfaceServices;


var builder = WebApplication.CreateBuilder(args);


// Add DbContext with MySQL
builder.Services.AddDbContext<TestapiContext>(options =>
    options.UseMySql(builder.Configuration.GetConnectionString("DefaultConnection"),
                     new MySqlServerVersion(new Version(8, 0, 21))));

// Add services to the container.
builder.Services.AddTransient<IProductionService, ProductionService>();


// Add repositories to the container.
builder.Services.AddTransient<IEstimateRepository, EstimateRepository>();


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();




var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment() || app.Environment.IsProduction())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
