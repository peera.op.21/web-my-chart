﻿using DataAccess.EFCore.Models;
using Microsoft.AspNetCore.Mvc;
using Services.InterfaceServices;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductionController : ControllerBase
    {

        private readonly IProductionService _productionService;
        public ProductionController(IProductionService productionService)
        {
            _productionService = productionService;
        }

        // GET: api/production/estimate
        [HttpGet("estimate")]
        public async Task<IActionResult> GetEstimates()
        {
            try
            {
                return Ok(await _productionService.GetEstimatesAsync());
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        // GET: api/production/estimate?year=2023
        [HttpGet("estimate/year/{year}")]
        public async Task<IActionResult> GetEstimatesByYear(int year)
        {
            try
            {
                return Ok(await _productionService.GetSumProductionsAsync(year));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        // POST: api/production/insert
        [HttpPost("insert")]
        public async Task<IActionResult> InsertEstimate([FromBody] Estimate estimate)
        {
            try
            {
                await _productionService.AddEstimateAsync(estimate);
                return Created();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        // POST: api/production/update
        [HttpPost("update")]
        public async Task<IActionResult> UpdateEstimate([FromBody] Estimate estimate)
        {
            try
            {
                await _productionService.UpdateEstimateAsync(estimate);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        // POST: api/production/delete
        [HttpPost("delete")]
        public async Task<IActionResult> DeleteEstimate(int id)
        {
            try
            {
                var estimate = await _productionService.GetEstimateAsync(id);
                if (estimate == null)
                {
                    return NotFound();
                }
                await _productionService.DeleteEstimateAsync(estimate);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


    }
}
