﻿using Bogus;
using DataAccess.EFCore.Models;
using Microsoft.AspNetCore.Mvc;
using Services.InterfaceServices;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeedController : ControllerBase
    {
        private readonly IProductionService _productionService;

        public SeedController(IProductionService productionService)
        {
            _productionService = productionService;
        }

        [HttpPost("generate-data")]
        public async Task<IActionResult> GenerateData([FromQuery] int rowCount = 500)
        {
            var faker = new Faker();

            var estimates = new List<Estimate>();

            var countries = new[] { "USA", "China", "Brazil", "EU", "India", "Russia" };
            var productionTypes = new[] { "Corn", "Wheat" };
            var startDate = new DateTime(2020, 1, 1);
            var endDate = new DateTime(2024, 12, 31);

            for (int i = 0; i < rowCount; i++)
            {
                var estimate = new Estimate
                {
                    Country = faker.PickRandom(countries),
                    ProductionType = faker.PickRandom(productionTypes),
                    ProductionAmount = faker.Random.Int(50, 500),
                    ProductionDate = faker.Date.Between(startDate, endDate)
                };

                estimates.Add(estimate);
            }

            await _productionService.AddEstimatesAsync(estimates);

            return Ok(new { Message = $"{rowCount} rows of sample data have been generated." });
        }
    }
}
