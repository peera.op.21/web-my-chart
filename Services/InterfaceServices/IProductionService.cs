﻿using DataAccess.EFCore.ComplexModels;
using DataAccess.EFCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.InterfaceServices
{
    public interface IProductionService
    {
        Task AddEstimateAsync(Estimate estimate);
        Task AddEstimatesAsync(List<Estimate> estimates);
        Task DeleteEstimateAsync(Estimate estimate);
        Task<Estimate?> GetEstimateAsync(int id);
        Task<List<Estimate>> GetEstimatesAsync();
        Task<List<SumProduction>> GetSumProductionsAsync(int year);
        Task UpdateEstimateAsync(Estimate estimate);
    }
}
