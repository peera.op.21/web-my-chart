﻿using DataAccess.EFCore.ComplexModels;
using DataAccess.EFCore.InterfaceRepositories;
using DataAccess.EFCore.Models;
using Services.InterfaceServices;

namespace Services
{
    public class ProductionService : IProductionService
    {
        private readonly IEstimateRepository _estimateRepository;
        public ProductionService(IEstimateRepository estimateRepository)
        {
            _estimateRepository = estimateRepository;
        }

        public async Task<List<Estimate>> GetEstimatesAsync() => await _estimateRepository.GetEstimatesAsync();

        public async Task<Estimate?> GetEstimateAsync(int id) => await _estimateRepository.GetEstimateAsync(id);

        public async Task AddEstimateAsync(Estimate estimate) => await _estimateRepository.AddAsync(estimate);

        public async Task AddEstimatesAsync(List<Estimate> estimates) => await _estimateRepository.AddRangeAsync(estimates);

        public async Task UpdateEstimateAsync(Estimate estimate) => await _estimateRepository.UpdateAsync(estimate);

        public async Task DeleteEstimateAsync(Estimate estimate) => await _estimateRepository.DeleteAsync(estimate);

        public async Task<List<SumProduction>> GetSumProductionsAsync(int year) => await _estimateRepository.GetSumProductionByYearAsync(year);
    }
}
