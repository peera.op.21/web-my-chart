﻿using DataAccess.EFCore.ComplexModels;
using DataAccess.EFCore.InterfaceRepositories;
using DataAccess.EFCore.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.EFCore.Repositories
{
    public class EstimateRepository : IEstimateRepository
    {
        private readonly TestapiContext _context;
        public EstimateRepository(TestapiContext context)
        {
            _context = context;
        }

        public async Task<List<Estimate>> GetEstimatesAsync() => await _context.Estimates.ToListAsync();


        public async Task<Estimate?> GetEstimateAsync(int id) => await _context.Estimates.FindAsync(id);


        public async Task<List<SumProduction>> GetSumProductionByYearAsync(int year)
        {
            return await _context.Estimates.GroupBy(g => g.Country).Select(s => new SumProduction
            {
                Country = s.Key,
                Productions = s.GroupBy(gx => gx.ProductionType).Select(sx => new DataAccess.EFCore.ComplexModels.Production
                {
                    ProductionType = sx.Key,
                    Sum = sx.Where(w => w.ProductionDate.Year == year).Sum(su => su.ProductionAmount),
                }).ToList()
            }).ToListAsync();
        }


        public async Task AddRangeAsync(List<Estimate> estimates)
        {
            await _context.Estimates.AddRangeAsync(estimates);
            await _context.SaveChangesAsync();
        }

        public async Task AddAsync(Estimate estimate)
        {
            await _context.Estimates.AddAsync(estimate);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Estimate estimate)
        {
            _context.Entry(estimate).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Estimate estimate)
        {
            _context.Estimates.Remove(estimate);
            await _context.SaveChangesAsync();
        }
    }
}
