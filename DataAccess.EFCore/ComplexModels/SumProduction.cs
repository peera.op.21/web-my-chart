﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.EFCore.ComplexModels
{
    public class SumProduction
    {
        public string? Country { get; set; }
        public List<Production>? Productions { get; set; }
    }

    public class Production
    {
        public string? ProductionType { get; set; }
        public double? Sum { get; set; }
    }
}
