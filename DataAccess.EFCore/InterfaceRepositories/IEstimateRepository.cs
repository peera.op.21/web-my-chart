﻿using DataAccess.EFCore.ComplexModels;
using DataAccess.EFCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.EFCore.InterfaceRepositories
{
    public interface IEstimateRepository
    {
        Task AddAsync(Estimate estimate);
        Task AddRangeAsync(List<Estimate> estimates);
        Task DeleteAsync(Estimate estimate);
        Task<Estimate?> GetEstimateAsync(int id);
        Task<List<Estimate>> GetEstimatesAsync();
        Task<List<SumProduction>> GetSumProductionByYearAsync(int year);
        Task UpdateAsync(Estimate estimate);
    }
}
